#ifndef SCHEDULER_H_INCLUDED
#define SCHEDULER_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>

/** Numero de procesos*/
int n;
/** Contador*/
int i;
/** Contiene numero de procesos*/
int p[20];

/** Pila donde se almacenan los datos*/
typedef struct Tarea{
    /** Valor de la prioridad*/
    int prioridad;
    /** Nombre de la tarea*/
	char nombre[20];
	/** Puntero que apunta al siguiente*/
	struct Tarea* siguiente;
}Tarea;
/** Puntero que apunta al primero*/
Tarea* primero = NULL;

void insertarTarea();

void desplegarPila();

#endif // SCHEDULER_H_INCLUDED
